+++
date = "2015-08-20T22:58:21+08:00"
draft = true
title = "Host RPM sites"
+++

Repo Server
=====

Create the local repo
------

	$ mkdir -p /home/www/rpm
	$ cp *.rpm /home/www/rpm
	$ createrepo -v /home/www/rpm
	
We can add more RPM packages later:

	$ cp *.rpm /home/www/rpm
	$ createrepo --update -v /home/www/rpm

Serve repo via nginx
------

Add a new site file:

	# /etc/sites/sites-available/rpm
	server {
	        listen 180.0.1.4:80;
	
	        server_name mu04;
	
	        root /home/repo/pi_rpm_repo;
	        autoindex on;
	}

Reload nginx service:

	# service nginx reload

Open the link in browser to check the status <http://mu04/>.

Yum Client
======

Enable the private repo
------

Add a new repo named ```pi.repo```:

	#/etc/yum.repos.d/pi.repo
	[pi]
	name=RPM Repo on Pi Supercomputer
	baseurl=http://mu04/
	enabled=1
	gpgcheck=0

Update metadata and check the repo list:

	# yum check-update
	# yum repolist | grep pi
	pi	RPM Repo on Pi Supercomputer	12
	# yum --disablerepo="*" --enablerepo="pi" list available

Enable higher priority on the local repo
------

Install the priority plugin for yum:

	# yum install yum-plugin-priorities
	
Make sure this plugin is enabled in ```/etc/yum/pluginconf.d/priorities.conf```

	[main]
	enabled=1

Specify priority for repos, with "1" the highest and "99" the lowest. For example,

	[pi]
	name=RPM Repo on Pi Supercomputer
	baseurl=http://mu04/
	enabled=1
	gpgcheck=0
	priority=2

List all the available packages from the "pi" repo:

	# yum --disablerepo="*" --enablerepo="pi" list available

Reference
======
* "Cent OS Wiki: 创建本地仓库" <https://wiki.centos.org/zh/HowTos/CreateLocalRepos>
* "Cent OS Wiki: yum-plugin-priorities"

