+++
date = "2018-05-19T09:43:41+08:00"
draft = false
title = "Spack Pakcage Manager"
+++

Install Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Install a library via Spack
======

Install a library via Spack in which `libxml2` is an example.

    $ spack install libxml2 %gcc@5.4.0

Load an installed library via Spack
======

Load an installed library via Spack in which `libxml2` is an example.

	$ source <(spack module tcl loads --dependencies libxml2)

A SLURM job sample in which libraries from Spack are used
======

	#!/bin/bash
	
	#SBATCH -J Spack
	#SBATCH -p cpu
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -n 1
    
    ulimit -l unlimited
    ulimit -s unlimited
    
	source /usr/share/Modules/init/bash
	module purge

    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi
    
	source <(spack module tcl loads --dependencies libxml2)

	./YOUR_APP

Reference
======
* "Spack package manager"<https://spack.io>
