+++
date = "2018-05-04T09:43:41+08:00"
draft = false
title = "GNU Scientific Library"
+++

Buld dependency packages via Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	


Log out, re-login then build gsl against GCC 5.4.0
------

	$ spack install gsl@2.4 %gcc@5.4.0

Load dependency libraries via Spack
------

	$ source <(spack module tcl loads --dependencies gsl@2.4 %gcc@5.4.0)

A SLURM job sample for GSL
======

Usage: `sbatch job.slurm`

	#!/bin/bash
	
	#SBATCH -J GSL
	#SBATCH -p cpu
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -n 1
	#SBATCH --exclusive

	source /usr/share/Modules/init/bash
	module use /lustre/usr/modulefiles
	module purge
	
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi

	source <(spack module tcl loads gsl@2.4 %gcc@5.4.0)

	./YOUR_GSL_APP
