+++
date = "2016-10-19T16:43:41+08:00"
draft = false
title = "R"
+++

This document shows you how to use Miniconda to up customized R environment in your `$HOME` directory.
**The spack way is still effective, but not recommended anymore.**

Install with Miniconda
======

Miniconda 3
======

Load Miniconda version 3.

```bash
$ module purge
$ module load miniconda3/4.5
```

Create a conda envirionment.

```bash
$ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/free/
$ conda config --add channels https://mirrors.sjtug.sjtu.edu.cn/anaconda/pkgs/main/
$ conda config --set show_channel_urls yes
$ conda create --name R
```

Activate your R environment.

```bash
$ source activate R
```

Install R.

```bash
conda install -c r r
```

Add more packages via `conda` like RMySQL.

```bash
$ conda install -c conda-forge r-rmysql
```

You can search for the installation command in https://anaconda.org/

Install with spack
======

Install Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Build R via Spack
======

    $ spack install r+external-lapack+X %gcc@5.4.0 ^openblas threads=openmp  ^cairo+X+pdf

Load R and install R modules
======

Load R.

    $ source <(spack module tcl loads --dependencies r+external-lapack+X %gcc@5.4.0 ^openblas threads=openmp ^cairo+X+pdf)
    $ which R
    
Delete existing R libs.

    $ rm -rf ~/.R* ~/Rlib*

Install R modules.

    $ source <( spack module tcl loads --dependencies r %gcc@5.4.0 )
    $ R --version
    R version 3.5.0 (2018-04-23) -- "Joy in Playing"
    Copyright (C) 2018 The R Foundation for Statistical Computing
    Platform: x86_64-pc-linux-gnu (64-bit)
    > R
    > source("https://bioconductor.org/biocLite.R")                    
    ...  
    Bioconductor version 3.7 (BiocInstaller 1.30.0), ?biocLite for help
    > biocLite()                  
    ...

A SLURM job sample for R
======

Usage: `sbatch job.slurm`

	#!/bin/bash
	
	#SBATCH -J R
	#SBATCH -p cpu
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -n 1
	#SBATCH --exclusive

	source /usr/share/Modules/init/bash
	
    # Spack
	if [ -d "$HOME/spack" ]; then
	        export SPACK_ROOT=$HOME/spack
	        source $SPACK_ROOT/share/spack/setup-env.sh
	fi

    source <(spack module tcl loads --dependencies r+external-lapack+X %gcc@5.4.0 ^openblas threads=openmp ^cairo+X+pdf)

	R hello.r

Install R modules in Spaco
======

Some R modules are available in Spack, for example, `r-devtools`. To install `r-devtools` with Spack, try:

    $ spack install r-devtools %gcc@5.4.0 r+external-lapack+X ^openblas threads=openmp ^cairo+X+pdf

To use `r-devtools` installed with Spack, load the following module:

    $ source <(spack module tcl loads --dependencies r-devtools)

Install R modules with external dependencies
======

To take `hdf5r` as an exmaple, which requires `hdf5@1.10.1`. First, build `hdf5@1.10.1`:

    $ spack install hdf5@1.10.1+hl+fortran+shared~szip~mpi~cxx %gcc@5.4.0 

Load R anad hdf5 modules.

    $ source <( spack module tcl loads gcc@5.4.0 )
    $ source <(spack module tcl loads --dependencies r+external-lapack+X %gcc@5.4.0 ^openblas threads=openmp ^cairo+X+pdf)
    $ source <( spack module tcl loads --dependencies hdf5 %gcc@5.4.0 )
    $ which R
    
Install R modules.

    $ R
    > install.packages("hdf5r")  
