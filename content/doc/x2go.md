+++
date = "2017-07-18T10:50:48+08:00"
title = "Remote Desktop via X2Go"
weight = 50
+++

X2Go is a remote desktop software available on Linux, Mac OS X, and Windows platforms.
It is released under GPL and can be downloaded on [X2Go official site](http://wiki.x2go.org).
This guide will show you how to connect to Pi supercomputer via X2Go client.

Download the X2Go installer from the website above. And you can install the software following the instruments.
 After installing the software.

Then create a new session by clicking "Session" -> "New Session".
Configure this session to use IP and port as the SSH service.

| Login node | OS       | IP             | SSH Port | Session typ | Software                     | GPU        |
|------------|----------|----------------|----------|-------------|------------------------------|------------|
| mu05       | CentOS 7 | 202.120.58.229 | 22       | MATE        | Intel Compiler               | None       |
| mu06       | CentOS 7 | 202.120.58.230 | 22       | MATE        | Intel Compiler, MATLAB, IDL  | NVIDIA K80 |
| mu07       | CentOS 7 | 202.120.58.231 | 22       | MATE        | Intel Compiler, PGI Compiler | NVIDIA K80 |

![Configure a session to connect to mu05](/images/x2gosession.png)

Click the newly-created session then input your username and password to login.

![Login via SSH username and password](/images/x2gologin.png)

![MATE desktop environment on x2go](/images/x2gomate.png)

After completing your work, terminate the session.

![Terminate X2GO session](/images/x2goexit.png)

In case of a hanging session, please terminate the current session then login again.
