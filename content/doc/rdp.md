+++
date = "2015-10-28T21:50:48+08:00"
draft = true
title = "Remote Deskto via NoMachine"
+++

NoMachine is a remote desktop software freely available on Linux, Mac OS X, and Windows platforms. It can be downloaded from [NoMachine official site](https://www.nomachine.com/download). This guide will show you how to connect to Pi supercomputer via NoMachine client.

![Create a new connection using SSH protocol](/images/nomachine1.png)

First, a new connection using ```SSH``` protocol should be created.

![Create a new connection using SSH protocol](/images/nomachine2.png)

Then fill the destination host with one of the following addresses: ```202.120.58.229```, ```202.120.58.230```, or ```202.120.58.231```, with port number ```22```.

![Fill the destination host and port](/images/nomachine3.png)

In the ```Authentication``` tab, be sure to select ```Use the NoMachine login``` option. Click ```[OK]``` and leave the server key empty.

![Select the "Use the NoMachine login" option](/images/nomachine4.png)
![Select the "Use the NoMachine login" option](/images/nomachine_nokey.png)

Leave the proxy page as "Don't use a proxy" as no proxy is required.

![No proxy is required](/images/nomachine_noproxy.png)

And name this configuration as one you prefer. 

![No proxy is required](/images/nomachine_name.png)

Double-click the configuration, input your username and password for Pi SSH login. Select ```[Create a new GNOME virtual Desktop]``` and you can enjoy a full-featured desktop now.

![No proxy is required](/images/nomachine5.png)

If you encounter issues on desktop resolution, you can set it manually on GNOME: ```[System]``` -> ```[Preferences]``` -> ```[Display]```. Pick the right resolution and click ```Alt+A``` to apply the changes.

![No proxy is required](/images/nomachine6.png)
