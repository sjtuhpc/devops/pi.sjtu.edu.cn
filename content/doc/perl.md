+++
date = "2018-01-15T09:43:41+08:00"
draft = false
title = "Perl"
+++

Buld dependency packages via Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Configure CPAN (Comprehensive Perl Archive Network) on Pi
======

Install Perl via spack.
------

    $ spack install perl %gcc@5.4.0
 
Delete existing CPAN modules if necessray.

	$ rm -rf ~/.perl ~/.cpan
   
Run CPAN configuration

    $ source <( spack module tcl loads perl %gcc@5.4.0 )
    $ cpan

Add the following settings to your `.bash_profile`:

	export PATH="$HOME/perl5/bin${PATH:+:${PATH}}"
	export PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}":$HOME/.perl/modules
	export PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
	export PERL_MB_OPT="--install_base \"$HOME/perl5\""
	export PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"

Log out then in to make the settings take effects.

Install Perl modules in CPAN
======

Synosys
------

    $ source <( spack module tcl loads --dependencies perl %gcc@5.4.0 )
    $ cpan
    cpan> install MODULE_NAME

Examples
------

    $ source <( spack module tcl loads gcc@5.4.0 )
    $ source <( spack module tcl loads --dependencies perl %gcc@5.4.0 )
    $ cpan
    cpan> install XML::LibXML
    ...
    cpan> install Getopt::Std
    ...
    cpan> install Encode

A SLURM job sample for Perl
======

Usage: `sbatch job.slurm`

	#!/bin/bash
	
	#SBATCH -J Perl
	#SBATCH -p cpu
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -n 1
    
	source /usr/share/Modules/init/bash
	
    # Spack
	if [ -d "$HOME/spack" ]; then
	        export SPACK_ROOT=$HOME/spack
	        source $SPACK_ROOT/share/spack/setup-env.sh
	fi
	
	source <(spack module tcl loads --dependencies perl %gcc@5.4.0 )

	perl hello.pl

Reference
======
* <http://www.perlmonks.org/?node_id=630026>
* <https://wiki.hpcc.msu.edu/display/Bioinfo/Installing+Local+Perl+Modules+with+CPAN>
