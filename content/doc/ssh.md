+++
date = "2015-11-23T08:36:54-07:00"
draft = false
title = "Log in The HPC Cluster via SSH"
weight = 10
+++

This document will introduce how to remotely login to the HPC cluster at SJTU via SSH.
Before reading this document, you need to have a good understanding of "Linux/Unix", "Terminal", "MS-DOS" and "SSH remote login". Or, you can read the References for more information about these concepts.

This document contains:

* The NOTES of using SSH to log in the cluster;
* The preparation of first login such as information collection, client downloading, SSH login, SSH file transfers and password-less login, etc;
* The troubleshooting and feedback.

Follwing the document's operation and feedback's methods will help you complete the job successfully. It will be very kind to provide advices, thank you!

Notices
======

* **The SSH accounts login to the cluster are only available for applicants and their colleagues in the same lab, you cannot lend the accounts to others.**
* **Please keep your SSH account and password carefully and don't share it with others. The staffs of HPCC won't ask for your SSH password.**
* **Malicious SSH clients, especially some "sinicizing client", may steal your SSH password^[Report about Chinese version Putty backdoor incident, please refer to <http://www.cnbeta.com/articles/171116.htm>], please use the English version SSH software recommended by [Client Downloading].**
* **Please don't jump to log in other nodes after you log in the HPC cluster. Shut down the SSH session when you finished your job.**
* **If you type the wrong password repeatedly, or use the ip address not in the "white list", you may not log in successfully. Please refer to [The troubleshooting and feedback], and send the diagnostic message to the administrator<support@lists.hpc.sjtu.edu.cn>.**

Preparation
======

Information Collection
------

When you log in the cluster via SSH, you need to fill in the server's ip address(or hostname), SSH port, SSH username and SSh password in the client. After the administrator allocate the account for you, we will send an email to inform you, please check it to get the informatin.
The email will be like this:

    SSH login node: TARGET_IP
    Username: YOUR_USERNAME
    Password: YOUR_PASSWORD
    Home: /lustre/home/YOUR_HOME

The detail is:

* SSH username: ```YOUR_USERNAME```
* SSH password: ```YOUR_PASSWORD```
* SSH login node's IP address: ```TARGET_IP```
* user's home folder: ```/lustre/home/YOUR_HOME```

*Note: To facilitate the narrative, the left content will follow the information above. In the actual operation, please refer to your email recieved and take care of your login information.*

Client Downloading
------

### Windows

Windows user can use ```putty```. It is a free and healty SSH client, you can run it by double clicking after downloading. ```putty``` can be downloaded from its homepage <http://www.putty.org/>.

### Linux/Unix/Mac

*Nix opration system like Linux/Unix/Mac has its own SSH client including ```ssh```, ```scp```, ```sftp```, etc. There is no need to install another software.

Log in the Cluster via SSH
======

Windows User
------

After starting up Putty,  please fill in the SSH login node's IP address(```IP address```), SSH port(```port```), then press ```Open```, as Figure 1.

![Fill in the SSH address and port](/images/putty1.png)

In the terminal window, type in your SSH username and password to log in, as Figure 2. 

*Note: when you are typing in the password, there is no ```*``` character to echo, please do it as usual and press 'enter' to log in.*

![Type in the username and password in putty terminal window](/images/putty2.png)

Linux/Unix/Mac User
------

Linux/Unix/Mac user can use the command line tools in terminal to log in. The instruction below figures out the node's IP address, username and SSH port.

    $ ssh YOUR_USERNAME@TARGET_IP

Transfer the Files via SSH
======

Windows User
------

Windows user can use WinSCP to transfer files between the cluster and your own computer. As the figure below, fill in the node's address(```Host name```), SSH port(```Port number```), SSH username(```User name```), SSH password(```Password```), then press ```Login``` to connect. The method of using WinSCP is like using FTP client GUI, as Figure 4.

![Fill in the SSH connection argument in WinSCP](/images/winscp.png)

![The operation interface of WinSCP](/images/winscp2.png)

Linux/Unix/Mac User
------

*NIX user can use the command line tools to transfer the data between the cluster and your own computer. The instruction below will upload the folder ```data/``` to the home folder's ```tmp/```.

    $ scp -r data/ YOUR_USERNAME@TARGET_IP:tmp/

The instruction below will download the home folder's ```data.out``` to the local present working directory.

    $ scp YOUR_USERNAME@TARGET_IP:data.out ./

If you want to finish more complicated data transfer operation, you can use ```sftp```. It just looks like FTP command line client.

    $ sftp YOUR_USERNAME@TARGET_IP
    Connected to TARGET_IP
    sftp> ls 

Password-less Login
======

*Note: "Password-less login" is only available to Linux/UNIX/Mac user who uses SSH command line tools. *

"Password-less login" lets you log in without type in the username and password, it can also make an alias of server to simplify the instruction. Password-less login need to establish the SSH trust relationship from the **remote host**(the cluster's login nodes) to the **local host**(your own computer). After the trust relationship is established, both sides will authenticate by SSH key-pair. For more information about SSH key-pair, please refer to [#Reference].

First, you need to generate your local-host's SSH key-pair. You can choose if using passphrases to protect the key-pair(suggest to choose "yes" and don't make the SSH password to be the passphrases). If you choose to use passphrases to protect the key-pair, you need to type the passphrases every time both sides authenticate. Mac operating system can remenber the passphrases automatically; Linux/UNIX user can use [keychain](https://wiki.gentoo.org/wiki/Keychain) to help manage the SSH password.

    $ ssh-keygen -t rsa

```ssh-keygen``` will generate a key-pair in ```~/.ssh```, ```id_rsa``` is the private key needed to be kept and ```id_rsa.pub``` is the public key which can be sent as your identity.

Then, use ```ssh-copy-id``` to add the local-host's public key ```id_rsa.pub``` to the remote-host's trust-list. In fact, what ```ssh-copy-id``` does is to add the ```id_rsa.pub```'s content to the remote-host's file ```~/.ssh/authorized_keys```.

    $ ssh-copy-id YOUR_USERNAME@TARGET_IP

We can also write the connection arguments into ```~/.ssh/config``` to make it conciser and secreter. Newly-built or edit the file ```~/.ssh/config```:

    $ EDIT ~/.ssh/config

Append the below content. ```Host``` assign the remote host's alias, ```HostName``` is the true domain name or IP address of remote host, ```Port``` assign the SSH port, ```User``` assign SSH username.

    Host hpc
    HostName TARGET_IP
    User YOUR_USERNAME

You need to make sure the authority of this file is right:

    $ chmod 600 ~/.ssh/config

Then, you can log in the HPC cluster by just typing:

    $ ssh hpc


<a id="debug"></a>Debug SSH Login issues
======

There are many reason that may prevent you logging on to the HPC cluster. **IP addresses failling in too many attempts of logins will be blocked for two housrs.** During the blocking peroid, it is suggested to try other login nodes. If this issue persists after two hours, please contact the [administrator](support@lists.hpc.sjtu.edu.cn) and attech the following info.

0. Try all the login nodes, mu05(202.120.58.229), mu06(202.120.58.230) and mu07(202.120.58.231).
1. Check your IP address at the bottom of the webpage <https://net.sjtu.edu.cn>.
2. Use ```ping``` to check if the network connection between your laptop and Pi. Replace the `IP` with IP address of one the login nodes -- mu05, mu06 and mu07.

        $ ping IP

3. Use ```telnet``` to check he IP of a login node). Replace the `ip` with IP address of one the login nodes -- mu05, mu06 and mu07.

        $ telnet IP 22

4. Check the details of SSH connecting information. Replace the `ip` with IP address of one the login nodes -- mu05, mu06 and mu07. 

        $ ssh -v YOUR_USERNAME@ip

Please contact with the [administrator](support@lits.hpc.sjtu.edu.cn), and append the debug info (your IP, ping, telnet, ssh results) in the message.

References
======

* "UNIX Tutorial for Beginners" <http://www.ee.surrey.ac.uk/Teaching/Unix/>
* "Vbird's Linux Private Kitchen : SSH server" <http://vbird.dic.ksu.edu.tw/linux_server/0310telnetssh.php#ssh_server>
* "Simplify Your Life With an SSH Config File" <http://nerderati.com/2011/03/simplify-your-life-with-an-ssh-config-file/>
* "keychain: Set Up Secure Passwordless SSH Access For Backup Scripts" <http://www.cyberciti.biz/faq/ssh-passwordless-login-with-keychain-for-scripts/>
* "Use Putty Password to Rmote Login to OpenSSH" <http://www.linuxfly.org/post/175/>
