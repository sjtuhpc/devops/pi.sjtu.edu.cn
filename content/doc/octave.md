+++
date = "2018-01-15T09:43:41+08:00"
draft = false
title = "Octave, an opensource alternative to MATLAB"
+++

Buld dependency packages via Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install
    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	

Build octave
======

Build (may takes 2 hours):

    $ spack install octave+qt+opengl %gcc@5.4.0 ^openblas threads=openmp
 
Run Octave in CLI
======

Load Octave module:

    $ source <(spack module tcl loads octave %gcc@5.4.0)

Run Octave:

	$ octave
    >> printf("Hello, World!\n");
    Hello, World!
    
Run Octave in GUI
======

Connect to Pi Supercomputer via [`x2go`](https://pi.sjtu.edu.cn/doc/x2go), then open the `Terminal` app.

Load Octave module:

    $ source <(spack module tcl loads octave %gcc@5.4.0)

Launch Octave:

	$ octave

Lauch Octave/MATLAB scripts (m-files) in SLURM jobs
======

Say, there is a sample MATLAB job script `hello.m`.
The following SLURM job script `octave.slurm` can be submitted via `sbatch octave.slurm`.

	#!/bin/bash
	
	#SBATCH -J octave
	#SBATCH -p cpu
	#SBATCH --mail-type=end
	#SBATCH --mail-user=YOU@EMAIL.COM
	#SBATCH -o %j.out
	#SBATCH -e %j.err
	#SBATCH -n 1
	#SBATCH --exclusive
    
	source /usr/share/Modules/init/bash
	
    # Spack
	if [ -d "$HOME/spack" ]; then
	        export SPACK_ROOT=$HOME/spack
	        source $SPACK_ROOT/share/spack/setup-env.sh
	fi
	
	source <(spack module tcl loads octave %gcc@5.4.0 )
    
	octave hello.m  
