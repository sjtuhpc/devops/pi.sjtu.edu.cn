+++
date = "2018-09-28T14:43:41+08:00"
draft = false
title = "nwchem"
+++

Buld dependency packages via Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install

    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	


Install nwchem via spack.
------

    $ spack install nwchem %gcc@5.4.0 ^openmpi+pmi fabrics=verbs schedulers=slurm ^netlib-lapack ^cmake@3.9.6
    
Load nwchem
------

    $ source <( spack module tcl loads --dependencies nwchem %gcc@5.4.0 )
    
