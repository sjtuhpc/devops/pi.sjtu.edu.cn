+++
title = "Using Desmond and Maestro on Pi Supercomputer"
date =  2018-09-28T09:21:15+08:00
draft = false
+++

Desmond and Maetro
======

Download the pre-compiled Desmond and Maestro bundle package
------

1. Login to Pi via [X2GO](https://pi.sjtu.edu.cn/doc/x2go).
2. Register and login to Desmond website on https://www.schrodinger.com/desmond .
3. Download Free Maestro for Linux, which may take 1 hour.
 
Install Desmond on Pi supercomputer
------

1. Make a directory for Desmond in your home directory, say ~/Maestro and ~/Maestro/tmp

	$ mkdir -p ~/Maestro/tmp
	
2. Excute the following command：

	$ tar -zxvf ./Maestro_2018-3_Linux-x86_64_Academic.tar
	$ cd ~/Maestro_2018-3_Linux-x86_64_Academic
	$ ./INSTALL
	
The default directory is system path which you have no permission to modify. So here you should type in [your/absolute/home/path/like]/Maestro and [your/absolute/home/path]/Maestro/tmp for Install Directory. Click enter button and y button whenever you need.
 
After a while, Maestro will be installed in ~/Maestro. You can log in pi via X2GO to use it. Enjoy!

TrajectoryMergeAssist
======

Install `tkinter` dependency (`python+tk`) via Spack.tkinter

	$ spack install python+tk %gcc@5.4.0
    
Download tma_v1.2.tar.gz then extract it to `$HOME`.

	$ cd ~/tmp
    $ wget https://github.com/avimanyu786/TrajectoryMergeAssist/releases/download/v1.2/tma_v1.2.tar.gz
    $ tar -xvzf tma_v1.2.tar.gz -C $ HOME

Login via X2GO, load tk-enabled python module, set `TMAPATH` environment then launch `tma`.

	$ source <(spack module tcl loads --dependencies python+tk)
    $ export TMAPATH=$HOME/tma_v1.2/bin
    $ export PATH=$PATH:$TMAPATH
    $ tma
