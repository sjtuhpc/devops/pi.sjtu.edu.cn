+++
date = "2016-07-10T21:44:37+08:00"
draft = true
title = "Build OpenFOAM on Pi"
+++

**To download packages from Internet, please set the proxy first.**

	$ export http_proxy=http://proxy.pi.sjtu.edu.cn:3004/
	$ export https_proxy=http://proxy.pi.sjtu.edu.cn:3004/

### Build OpenFOAM 2.3 with GCC and OpenMPI

Download and extract OpenFOAM source code.

    $ mkdir ~/OpenFOAM
    $ cd ~/OpenFOAM
	$ wget http://nchc.dl.sourceforge.net/project/foam/foam/2.3.1/OpenFOAM-2.3.1.tgz
	$ tar -xzvf OpenFOAM-2.3.1.tgz

Configure dependency modules in OpenFOAM source tree.

	$ cd ~/OpenFOAM/OpenFOAM-2.3.1
    $ sed -i s/"WM_COMPILER=Gcc"/"WM_COMPILER=Gcc49"/g etc/bashrc
    $ sed -i s/"WM_MPLIB=OPENMPI"/"WM_MPLIB=SYSTEMOPENMPI"/g etc/bashrc
    $ sed -i s/"cgal_version=CGAL-4.3"/"cgal_version=CGAL-system"/g etc/config/CGAL.sh
    $ sed -i s/"SCOTCH_VERSION=scotch_6.0.0"/"SCOTCH_VERSION=scotch-system"/g etc/config/CGAL.sh

Load environment modules.

    $ module use /lustre/usr/modulefiles/pi
    $ module purge; module load gcc/4.9 openmpi/1.10 scotch/6.0 cgal/4.7

Build OpenFOAM.

	$ cd ~/OpenFOAM/OpenFOAM-2.3.1
    $ export WM_NCOMPPROCS=4
    $ source etc/bashrc
    $ cd $WM_PROJECT_DIR
	$ ./Allwmake > make.log 2>&1

Run `Allwmake` again to verify.

	$ ./Allwmake

Use OpenFOAM

    $ module purge; module load gcc/4.9 openmpi/1.10 scotch/6.0 cgal/4.7
	$ source ~/OpenFOAM/OpenFOAM-2.3.1/etc/bashrc
	$ icoFoam -h
