+++
date = "2018-07-19T09:43:41+08:00"
draft = false
title = "fftw"
+++

Buld dependency packages via Spack
======

	$ cd
	$ git clone https://github.com/sjtuhpcc/spack.git
    $ cd spack
    $ ./bootstrap.sh user --install

    
Add the following settings to ```~/.bashrc``` or ```~/.bash_profile```.

	# Spack package management
    if [ -d "$HOME/spack" ]; then
        export SPACK_ROOT=$HOME/spack
        source $SPACK_ROOT/share/spack/setup-env.sh
    fi	


Install fftw via spack.
------

    $ spack install fftw~mpi %gcc@5.4.0
    
Load fftw
------

    $ source <( spack module tcl loads --dependencies fftw~mpi %gcc@5.4.0 )
    
Install fftw2 via spack.
------
 
    $ spack install fftw2~mpi %gcc@5.4.0
    
Load fftw2
------

    $ source <( spack module tcl loads --dependencies fftw2~mpi %gcc@5.4.0 )
